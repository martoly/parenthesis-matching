package task03;

import java.util.Stack;

public class ParenthesesMatching {
    private String sequence;

    ParenthesesMatching(String sequence) {
        this.sequence = sequence;
    }

    boolean matching() {
        Stack<Character> s = new Stack<>();
        int length = sequence.length();
        char current;
        for (int i = 0; i < length; i++) {
            current = sequence.charAt(i);
            switch (current) {
                case '(':
                case '[':
                case '{':
                    s.push(current);
                    break;
                case ']':
                    if (s.peek() == '[') {
                        s.pop();
                    } else {
                        return false;
                    }
                    break;
                case '}':
                    if (s.peek() == '{') {
                        s.pop();
                    } else {
                        return false;
                    }
                    break;
                case ')':
                    if (s.peek() == '(') {
                        s.pop();
                    } else {
                        return false;
                    }
                    break;
            }

        }
        return s.empty();
    }
}
