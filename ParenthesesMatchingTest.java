package task03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParenthesesMatchingTest {

    @Test
    void test_empty_input() {
        String input = "";
        ParenthesesMatching p = new ParenthesesMatching(input);
        boolean actual = p.matching();
        Assertions.assertTrue(actual);
    }

    @Test
    void test_simple_correct_input() {
        String input = "([])";
        ParenthesesMatching p = new ParenthesesMatching(input);
        boolean actual = p.matching();
        Assertions.assertTrue(actual);
    }

    @Test
    void test_simple_incorrect_input() {
        String input = "{()";
        ParenthesesMatching p = new ParenthesesMatching(input);
        boolean actual = p.matching();
        Assertions.assertFalse(actual);
    }

    @Test
    void test_complicated_correct_input1() {
        String input = "{ int a = 42; }";
        ParenthesesMatching p = new ParenthesesMatching(input);
        boolean actual = p.matching();
        Assertions.assertTrue(actual);
    }

    @Test
    void test_complicated_correct_input2() {
        String input = "{ int b = calculate(); }";
        ParenthesesMatching p = new ParenthesesMatching(input);
        boolean actual = p.matching();
        Assertions.assertTrue(actual);
    }

    @Test
    void test_simple_correct_input2() {
        String input = "(){}[]";
        ParenthesesMatching p = new ParenthesesMatching(input);
        boolean actual = p.matching();
        Assertions.assertTrue(actual);
    }

    @Test
    void test_missmatching_brakets_input() {
        String input = "([)]";
        ParenthesesMatching p = new ParenthesesMatching(input);
        boolean actual = p.matching();
        Assertions.assertFalse(actual);
    }

    @Test
    void test_long_sequence_matching_brakets_input() {
        String input = "[([{}]){}([[]]{})]";
        ParenthesesMatching p = new ParenthesesMatching(input);
        boolean actual = p.matching();
        Assertions.assertTrue(actual);
    }
}
